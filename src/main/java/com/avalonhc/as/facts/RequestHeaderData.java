
package com.avalonhc.as.facts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RequestHeaderData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestHeaderData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idCardNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="healthPlanId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="healthPlanIdType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="EI"/>
 *               &lt;enumeration value="FI"/>
 *               &lt;enumeration value="FY"/>
 *               &lt;enumeration value="HI"/>
 *               &lt;enumeration value="NF"/>
 *               &lt;enumeration value="PI"/>
 *               &lt;enumeration value="XV"/>
 *               &lt;enumeration value="2U"/>
 *               &lt;enumeration value="AV"/>
 *               &lt;enumeration value="AN"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="healthPlanGroupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="claimNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="healthPlanMemberId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="patientLastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="patientFirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="patientMiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientSuffixName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientDateOfBirth" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="patientGenderCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="icdDiagVersQualifier">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="09"/>
 *               &lt;enumeration value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="primaryDiagnosisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diagnosisCodes" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="24" minOccurs="0"/>
 *         &lt;element name="billingProviderTaxId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="billingProviderNpi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="renderingProviderNpi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numberOfLines" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="billType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="blueCardCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="0"/>
 *               &lt;enumeration value="1"/>
 *               &lt;enumeration value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="fromDateOfService" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="toDateOfService" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="cobIndicator">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Y"/>
 *               &lt;enumeration value="N"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="excludeMembershipIndicator">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Y"/>
 *               &lt;enumeration value="N"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="lineOfBusiness">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="1"/>
 *               &lt;enumeration value="2"/>
 *               &lt;enumeration value="3"/>
 *               &lt;enumeration value="4"/>
 *               &lt;enumeration value="5"/>
 *               &lt;enumeration value="6"/>
 *               &lt;enumeration value="7"/>
 *               &lt;enumeration value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="trialClaimIndicator">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Y"/>
 *               &lt;enumeration value="N"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="originalClaimNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "requestHeaderData")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestHeaderData", propOrder = {
    "idCardNumber",
    "healthPlanId",
    "healthPlanIdType",
    "healthPlanGroupId",
    "claimNumber",
    "healthPlanMemberId",
    "patientLastName",
    "patientFirstName",
    "patientMiddleName",
    "patientSuffixName",
    "patientDateOfBirth",
    "patientGenderCode",
    "icdDiagVersQualifier",
    "primaryDiagnosisCode",
    "diagnosisCodes",
    "billingProviderTaxId",
    "billingProviderNpi",
    "renderingProviderNpi",
    "numberOfLines",
    "billType",
    "blueCardCode",
    "fromDateOfService",
    "toDateOfService",
    "cobIndicator",
    "excludeMembershipIndicator",
    "lineOfBusiness",
    "trialClaimIndicator",
    "originalClaimNumber",
    "uniqueMemberId"
})
public class RequestHeaderData implements Serializable {

    @XmlElement(required = true)
    protected String idCardNumber;
    @XmlElement(required = true)
    protected String healthPlanId;
    @XmlElement(required = true)
    protected String healthPlanIdType;
    protected String healthPlanGroupId;
    @XmlElement(required = true)
    protected String claimNumber;
    @XmlElement(required = true)
    protected String healthPlanMemberId;
    @XmlElement(required = true)
    protected String patientLastName;
    @XmlElement(required = true)
    protected String patientFirstName;
    protected String patientMiddleName;
    protected String patientSuffixName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar patientDateOfBirth;
    @XmlElement(required = true)
    protected String patientGenderCode;
    @XmlElement(required = true)
    protected String icdDiagVersQualifier;
    protected String primaryDiagnosisCode;
    protected List<String> diagnosisCodes;
    @XmlElement(required = true)
    protected String billingProviderTaxId;
    protected String billingProviderNpi;
    protected String renderingProviderNpi;
    protected int numberOfLines;
    protected String billType;
    @XmlElement(required = true)
    protected String blueCardCode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fromDateOfService;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar toDateOfService;
    @XmlElement(required = true)
    protected String cobIndicator;
    @XmlElement(required = true)
    protected String excludeMembershipIndicator;
    @XmlElement(required = true)
    protected String lineOfBusiness;
    @XmlElement(required = true)
    protected String trialClaimIndicator;
    protected String originalClaimNumber;
    
    protected String uniqueMemberId;

    /**
     * Gets the value of the idCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCardNumber() {
        return idCardNumber;
    }

    /**
     * Sets the value of the idCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCardNumber(String value) {
        this.idCardNumber = value;
    }

    /**
     * Gets the value of the healthPlanId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthPlanId() {
        return healthPlanId;
    }

    /**
     * Sets the value of the healthPlanId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthPlanId(String value) {
        this.healthPlanId = value;
    }

    /**
     * Gets the value of the healthPlanIdType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthPlanIdType() {
        return healthPlanIdType;
    }

    /**
     * Sets the value of the healthPlanIdType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthPlanIdType(String value) {
        this.healthPlanIdType = value;
    }

    /**
     * Gets the value of the healthPlanGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthPlanGroupId() {
        return healthPlanGroupId;
    }

    /**
     * Sets the value of the healthPlanGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthPlanGroupId(String value) {
        this.healthPlanGroupId = value;
    }

    /**
     * Gets the value of the claimNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimNumber() {
        return claimNumber;
    }

    /**
     * Sets the value of the claimNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimNumber(String value) {
        this.claimNumber = value;
    }

    /**
     * Gets the value of the healthPlanMemberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthPlanMemberId() {
        return healthPlanMemberId;
    }

    /**
     * Sets the value of the healthPlanMemberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthPlanMemberId(String value) {
        this.healthPlanMemberId = value;
    }

    /**
     * Gets the value of the patientLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientLastName() {
        return patientLastName;
    }

    /**
     * Sets the value of the patientLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientLastName(String value) {
        this.patientLastName = value;
    }

    /**
     * Gets the value of the patientFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientFirstName() {
        return patientFirstName;
    }

    /**
     * Sets the value of the patientFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientFirstName(String value) {
        this.patientFirstName = value;
    }

    /**
     * Gets the value of the patientMiddleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientMiddleName() {
        return patientMiddleName;
    }

    /**
     * Sets the value of the patientMiddleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientMiddleName(String value) {
        this.patientMiddleName = value;
    }

    /**
     * Gets the value of the patientSuffixName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientSuffixName() {
        return patientSuffixName;
    }

    /**
     * Sets the value of the patientSuffixName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientSuffixName(String value) {
        this.patientSuffixName = value;
    }

    /**
     * Gets the value of the patientDateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPatientDateOfBirth() {
        return patientDateOfBirth;
    }

    /**
     * Sets the value of the patientDateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPatientDateOfBirth(XMLGregorianCalendar value) {
        this.patientDateOfBirth = value;
    }

    /**
     * Gets the value of the patientGenderCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientGenderCode() {
        return patientGenderCode;
    }

    /**
     * Sets the value of the patientGenderCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientGenderCode(String value) {
        this.patientGenderCode = value;
    }

    /**
     * Gets the value of the icdDiagVersQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcdDiagVersQualifier() {
        return icdDiagVersQualifier;
    }

    /**
     * Sets the value of the icdDiagVersQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcdDiagVersQualifier(String value) {
        this.icdDiagVersQualifier = value;
    }

    /**
     * Gets the value of the primaryDiagnosisCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryDiagnosisCode() {
        return primaryDiagnosisCode;
    }

    /**
     * Sets the value of the primaryDiagnosisCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryDiagnosisCode(String value) {
        this.primaryDiagnosisCode = value;
    }

    /**
     * Gets the value of the diagnosisCodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the diagnosisCodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDiagnosisCodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDiagnosisCodes() {
        if (diagnosisCodes == null) {
            diagnosisCodes = new ArrayList<String>();
        }
        return this.diagnosisCodes;
    }

    /**
     * Gets the value of the billingProviderTaxId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingProviderTaxId() {
        return billingProviderTaxId;
    }

    /**
     * Sets the value of the billingProviderTaxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingProviderTaxId(String value) {
        this.billingProviderTaxId = value;
    }

    /**
     * Gets the value of the billingProviderNpi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingProviderNpi() {
        return billingProviderNpi;
    }

    /**
     * Sets the value of the billingProviderNpi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingProviderNpi(String value) {
        this.billingProviderNpi = value;
    }

    /**
     * Gets the value of the renderingProviderNpi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRenderingProviderNpi() {
        return renderingProviderNpi;
    }

    /**
     * Sets the value of the renderingProviderNpi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRenderingProviderNpi(String value) {
        this.renderingProviderNpi = value;
    }

    /**
     * Gets the value of the numberOfLines property.
     * 
     */
    public int getNumberOfLines() {
        return numberOfLines;
    }

    /**
     * Sets the value of the numberOfLines property.
     * 
     */
    public void setNumberOfLines(int value) {
        this.numberOfLines = value;
    }

    /**
     * Gets the value of the billType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillType() {
        return billType;
    }

    /**
     * Sets the value of the billType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillType(String value) {
        this.billType = value;
    }

    /**
     * Gets the value of the blueCardCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlueCardCode() {
        return blueCardCode;
    }

    /**
     * Sets the value of the blueCardCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlueCardCode(String value) {
        this.blueCardCode = value;
    }

    /**
     * Gets the value of the fromDateOfService property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFromDateOfService() {
        return fromDateOfService;
    }

    /**
     * Sets the value of the fromDateOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFromDateOfService(XMLGregorianCalendar value) {
        this.fromDateOfService = value;
    }

    /**
     * Gets the value of the toDateOfService property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getToDateOfService() {
        return toDateOfService;
    }

    /**
     * Sets the value of the toDateOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setToDateOfService(XMLGregorianCalendar value) {
        this.toDateOfService = value;
    }

    /**
     * Gets the value of the cobIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCobIndicator() {
        return cobIndicator;
    }

    /**
     * Sets the value of the cobIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCobIndicator(String value) {
        this.cobIndicator = value;
    }

    /**
     * Gets the value of the excludeMembershipIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExcludeMembershipIndicator() {
        return excludeMembershipIndicator;
    }

    /**
     * Sets the value of the excludeMembershipIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExcludeMembershipIndicator(String value) {
        this.excludeMembershipIndicator = value;
    }

    /**
     * Gets the value of the lineOfBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    /**
     * Sets the value of the lineOfBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineOfBusiness(String value) {
        this.lineOfBusiness = value;
    }

    /**
     * Gets the value of the trialClaimIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrialClaimIndicator() {
        return trialClaimIndicator;
    }

    /**
     * Sets the value of the trialClaimIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrialClaimIndicator(String value) {
        this.trialClaimIndicator = value;
    }

    /**
     * Gets the value of the originalClaimNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalClaimNumber() {
        return originalClaimNumber;
    }

    /**
     * Sets the value of the originalClaimNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalClaimNumber(String value) {
        this.originalClaimNumber = value;
    }

	public String getUniqueMemberId() {
		return uniqueMemberId;
	}

	public void setUniqueMemberId(String uniqueMemberId) {
		this.uniqueMemberId = uniqueMemberId;
	}

}
