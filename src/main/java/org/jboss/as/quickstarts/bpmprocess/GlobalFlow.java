package org.jboss.as.quickstarts.bpmprocess;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.kie.api.KieServices;
import org.kie.api.runtime.Environment;
import org.kie.api.runtime.EnvironmentName;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import org.kie.api.runtime.KieSession;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.Task;
import org.kie.api.task.model.TaskSummary;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.remote.jaxb.gen.FindVariableInstancesCommand;
import org.kie.remote.jaxb.gen.FireAllRulesCommand;
import org.kie.services.client.api.RemoteRuntimeEngineFactory;
import org.kie.services.client.api.command.RemoteRuntimeEngine;
import org.kie.services.client.serialization.jaxb.impl.audit.JaxbVariableInstanceLog;
import org.kie.services.client.serialization.jaxb.impl.task.JaxbTaskSummary;
import org.jbpm.process.audit.AbstractAuditLogger;
import org.jbpm.process.audit.AuditLoggerFactory;

import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.DateUtility;
import com.avalonhc.as.facts.EvaluateLabClaim;
import com.avalonhc.as.facts.EvaluateLabClaimResponse;
import com.avalonhc.as.facts.RequestHeaderData;
import com.avalonhc.as.facts.ResponseHeaderData;
import com.avalonhc.as.facts.ResponseLineData;
import com.avalonhc.as.facts.SecondaryCodes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlobalFlow {

	public EvaluateLabClaimResponse start(EvaluateLabClaim evaluateLabClaim) throws ParseException, IOException {

		Logger logger = LoggerFactory.getLogger(EvaluateLabClaimResponse.class);
		Map input = getMapFromEvaluateClaimRequest(evaluateLabClaim);
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		System.out.println("[GlobalFlow]-Class-start");

		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = loader.getResourceAsStream("jbpm_rest.properties");
		if (inputStream == null)
			System.out.println("Cannot locate properties file");

		Properties props = new java.util.Properties();
		props.load(inputStream);
		String user = props.getProperty("user");

		String password = props.getProperty("password");
		String deploymentId = props.getProperty("deploymentId");
		String processId = props.getProperty("processId");
		String applicationUrlString = props.getProperty("applicationUrlString");

		System.out.println("[GlobalFlow]-Getting Header Data");

		RequestHeaderData requestHeaderData = evaluateLabClaim.getRequestHeader();
		ResponseHeaderData responseHeaderData = new ResponseHeaderData();
		responseHeaderData.setClaimNumber(requestHeaderData.getClaimNumber());
		RuntimeEngine engine = null;
		System.out.println("[GlobalFlow]-Creating Engine");
		try {
			engine = RemoteRuntimeEngineFactory.newRestBuilder().addUrl(new URL(applicationUrlString)).addUserName(user)
					.addPassword(password).addDeploymentId(deploymentId)
					.addExtraJaxbClasses(ClaimRequest.class, EvaluateLabClaimResponse.class, SecondaryCodes.class,
							ResponseHeaderData.class, ResponseLineData.class, EvaluateLabClaim.class)
					.build();

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			responseHeaderData.getCeErrorCodes().add(new Integer("400"));
			System.out.println("[GlobalFlow]-In Engine Exception Block");
		}

		KieSession ksession = engine.getKieSession();
		ProcessInstance processInstance = null;

		try {

			System.out.println("[GlobalFlow]-Getting Process Instance");
			processInstance = ksession.startProcess(processId, input);
			System.out.println("[GlobalFlow] processInstance: " + processInstance.toString());

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getLocalizedMessage());
			System.out.println("[GlobalFlow]-In Process Instance Exception Block");
		}

		System.out.println("[GlobalFlow]-Getting Process ID");
		System.out.println("[GlobalFlow]ProcessId " + processInstance.getId());
		List varHeaderLog = engine.getAuditService().findVariableInstances(processInstance.getId(), "HeaderData");
		String reqHeader = "";
		System.out.println("[GlobalFlow]- Request Header retrieved");
		int lastHeader = varHeaderLog.size();
		System.out.println("[GlobalFlow]-varLogSize");
		if (lastHeader > 0) {
			System.out.println("[GlobalFlow]-In header > 0 block");
			JaxbVariableInstanceLog lastObjHeader = (JaxbVariableInstanceLog) varHeaderLog.get(lastHeader - 1);
			System.out.println("[GlobalFlow]-Class-start3.10");
			reqHeader = lastObjHeader.getValue();
			System.out.println("[GlobalFlow]w-Class-start3.11");
		}

		System.out.println("[GlobalFlow]-Class-start3.12");

		System.out.println("[GlobalFlow]-Class-start4");

		List varLog = engine.getAuditService().findVariableInstances(processInstance.getId(), "responseLineData");

		int last = varLog.size();
		System.out.println("VarLog size =" + last);

		JaxbVariableInstanceLog lastObj = (JaxbVariableInstanceLog) varLog.get(last - 1);

		// Amit Naresh - since the thing is in Rest - we get a string back.
		// Right now I saw only ResponseLine. I will have to add line for
		// response
		// header?

		System.out.println("[GlobalFlow]-Class-start5");

		String requestLine = lastObj.getValue();

		String removedResponseLineData = StringUtils.strip(requestLine, "ResponseLineData [");
		int numberSecondaryCodesData = StringUtils.indexOf(removedResponseLineData, ", secondaryCodesData=[");
		String primaries = StringUtils.left(removedResponseLineData, numberSecondaryCodesData);
		// System.out.println("primaries");
		// System.out.println(primaries);
		String secondaries = StringUtils.right(removedResponseLineData,
				removedResponseLineData.length() - numberSecondaryCodesData);
		// System.out.println("secondaries");
		// System.out.println(secondaries);
		String[] primaryArray = StringUtils.split(primaries, ",");

		System.out.println("[GlobalFlow]w-Class-start6");

		ResponseLineData responseLineData = new ResponseLineData();

		if (primaryArray != null && primaryArray.length > 0) {
			int lineNumberIndex = StringUtils.indexOf(primaryArray[0], "=");
			String lineNumberString = StringUtils.right(primaryArray[0],
					primaryArray[0].length() - (lineNumberIndex + 1));
			int lineNumber = Integer.parseInt(lineNumberString);

			int procedureCodeIndex = StringUtils.indexOf(primaryArray[1], "=");
			String procedureCode = StringUtils.right(primaryArray[1],
					primaryArray[1].length() - (procedureCodeIndex + 1));

			int primaryDecisionCodeIndex = StringUtils.indexOf(primaryArray[2], "=");
			String primaryDecisionCode = StringUtils.right(primaryArray[2],
					primaryArray[2].length() - (primaryDecisionCodeIndex + 1));

			int primaryPolicyTagIndex = StringUtils.indexOf(primaryArray[3], "=");
			String primaryPolicyTag = StringUtils.right(primaryArray[3],
					primaryArray[3].length() - (primaryPolicyTagIndex + 1));
			System.out.println("[GlobalFlow]-Class-start7");

			int primaryPolicyNecessityCriterionIndex = StringUtils.indexOf(primaryArray[4], "=");
			String primaryPolicyNecessityCriterion = StringUtils.right(primaryArray[4],
					primaryArray[4].length() - (primaryPolicyNecessityCriterionIndex + 1));

			responseLineData.setLineNumber(lineNumber);
			responseLineData.setProcedureCode(procedureCode);
			responseLineData.setPrimaryDecisionCode(primaryDecisionCode);
			responseLineData.setPrimaryPolicyTag(primaryPolicyTag);
			responseLineData.setPrimaryPolicyNecessityCriterion(primaryPolicyNecessityCriterion);
		}
		List varLog1 = engine.getAuditService().findVariableInstances(processInstance.getId(), "secondaryCodes");

		System.out.println("[GlobalFlow]-Class-start8");

		int last1 = varLog.size();
		JaxbVariableInstanceLog lastObj1 = (JaxbVariableInstanceLog) varLog1.get(last1 - 1);

		String secondaryString = lastObj1.getValue();
		// ------------------------do secondary ---
		int firstIndexSecondaryString = StringUtils.indexOf(secondaryString, "[");
		String secondaryCodesString = StringUtils.right(secondaryString,
				secondaryString.length() - (firstIndexSecondaryString + 1));
		int secondIndexSecondaryString = secondaryCodesString.indexOf("]");
		secondaryCodesString = StringUtils.left(secondaryCodesString, secondIndexSecondaryString);
		logger.info("secondary Code String  = " + secondaryCodesString);
		String[] secondaryArray = StringUtils.split(secondaryCodesString, ",");
		System.out.println("ZZZZZ-GlobalFlow-Class-start9");

		if (secondaryArray != null && secondaryArray.length > 0) {
			SecondaryCodes secondaryCodes = new SecondaryCodes();
			System.out.println("ZZZZZ-GlobalFlow-After-Instantiation");

			int secondaryDecisionCodeIndex = StringUtils.indexOf(secondaryArray[0], "=");
			String secondaryDecisionCode = StringUtils.right(secondaryArray[0],
					secondaryArray[0].length() - (secondaryDecisionCodeIndex + 1));

			int secondarPolicyTagIndex = StringUtils.indexOf(secondaryArray[1], "=");
			String secondaryPolicyTag = StringUtils.right(secondaryArray[1],
					secondaryArray[1].length() - (secondarPolicyTagIndex + 1));
			secondaryCodes.setSecondaryDecisionCode("D11R");
			secondaryCodes.setSecondaryPolicyTag("20416");
			secondaryCodes.setSecondaryPolicyTag("20416");

			int secondarPolicyNecessityIndex = StringUtils.indexOf(secondaryArray[2], "=");
			String secondarPolicyNecessity = StringUtils.right(secondaryArray[2],
					secondaryArray[2].length() - (secondarPolicyNecessityIndex + 1));
			secondaryCodes.setSecondaryPolicyNecessityCriteria(secondarPolicyNecessity);
			System.out.println("ZZZZZ-GlobalFlow-Class-start10");
			System.out.println("ZZZZZ Secondary Codes:" + secondaryCodes);
			System.out.println("ZZZZZ-GlobalFlow-Class-start10.5");

			responseLineData.getSecondaryCodesData().add(secondaryCodes);
		}
		EvaluateLabClaimResponse evaluateLabClaimResponse = new EvaluateLabClaimResponse();
		evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
		evaluateLabClaimResponse.getResponseLine().add(responseLineData);
		// return evaluateLabClaimResponse;

		return evaluateLabClaimResponse;
	}

	Map getMapFromEvaluateClaimRequest(EvaluateLabClaim evaluateLabClaim) throws ParseException {
		Map<String, Object> params = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		ClaimRequest claimRequest = new ClaimRequest();
		System.out.println("ZZZZZ healthpland:::" + evaluateLabClaim.getRequestHeader().getHealthPlanId());
		if (null != evaluateLabClaim.getRequestHeader().getHealthPlanId()
				&& !"".equals(evaluateLabClaim.getRequestHeader().getHealthPlanId())
				&& !"?".equals(evaluateLabClaim.getRequestHeader().getHealthPlanId())) {
			claimRequest.setHealthPlanId(evaluateLabClaim.getRequestHeader().getHealthPlanId().trim());
			System.out.println("ZZZZZ Health plan:::" + evaluateLabClaim.getRequestHeader().getHealthPlanId());
		}

		System.out.println("ZZZZZ-GlobalFlow-Class-start11");

		if (null != evaluateLabClaim.getRequestLine().get(0).getProcedureCode()
				&& !"".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode())
				&& !"?".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode())) {
			claimRequest.setProcedureCode(evaluateLabClaim.getRequestLine().get(0).getProcedureCode().trim());
			// System.out.println("procedure code:::"+
			// evaluateLabClaim.getRequestLine().get(0).getProcedureCode());
		}

		if (null != evaluateLabClaim.getRequestLine().get(0).getFromDateOfService()
				&& !"".equals(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService())
				&& !"?".equals(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService())) {

			claimRequest.setFromDateOfService(
					sdf.parse(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService().toString().trim()));
			// claimRequest.setFromDateOfService(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService());
			System.out.println("From Date::" + evaluateLabClaim.getRequestLine().get(0).getFromDateOfService());
		}

		System.out.println("DOB::" + evaluateLabClaim.getRequestHeader().getPatientDateOfBirth());
		System.out.println("primaryDiagnosis:::" + evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode());

		if (null != evaluateLabClaim.getRequestLine().get(0).getPlaceOfService()
				&& !"".equals(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService())
				&& !"?".equals(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService())) {
			claimRequest.setPlaceOfService(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService().trim());
			System.out.println("PlaceOfService::::" + evaluateLabClaim.getRequestLine().get(0).getPlaceOfService());
		}
		// setting PrimaryDiagnosisCode and DiagnosisCode in List for comparing
		// with single column value in decision table
		claimRequest.setError("NO");
		List<String> diagnosisCodesList = new ArrayList<String>();
		int DiagnosisCodePointersCount = 0;

		if (null == evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode()
				|| "".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())
				|| "?".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())) {
			if (null != evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0)
					&& !"".equals(evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0))) {
				DiagnosisCodePointersCount = Integer
						.parseInt(evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0));
				claimRequest.setDiagnosisCodePointers(DiagnosisCodePointersCount);
			}
		}
		int count = evaluateLabClaim.getRequestHeader().getDiagnosisCodes().size();

		System.out.println("Step 1 diagnosisCodesList ---->");

		if (null != evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode()
				&& !"".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())
				&& !"?".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())) {
			diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode().trim());
			claimRequest.setPrimaryDiagnosisCode(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode().trim());
		}
		System.out.println("in for loop diagnosiscode list" + evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
		System.out.println("in for loop the value of count1" + count);
		if (null == evaluateLabClaim.getRequestHeader().getDiagnosisCodes()
				|| "".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())
				|| "?".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())) {
			System.out.println(
					"in for loop diagnosiscode list" + evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
			System.out.println("in for loop the value of count" + count);
			for (int i = 0; i < count; i++) {
				System.out.println("in for loop the value of i" + i);
				diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i).trim());
				System.out.println("in for loop diagnosiscode list"
						+ evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
			}

		}
		System.out.println("Step 1 diagnosisCodesList ---->");
		if (null != evaluateLabClaim.getRequestHeader().getDiagnosisCodes()
				&& !"".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())
				&& !"?".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())) {
			// diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
			for (int i = 0; i < evaluateLabClaim.getRequestHeader().getDiagnosisCodes().size(); i++) {
				System.out.println(
						"diagnosisCodesList ---->" + evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
				diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i).trim());
			}

		}
		System.out.println("diagnosisCodesList ---->" + diagnosisCodesList);
		claimRequest.getDiagnosisCodesList().addAll(diagnosisCodesList);

		if (null != evaluateLabClaim.getRequestHeader().getPatientGenderCode()
				&& !"".equals(evaluateLabClaim.getRequestHeader().getPatientGenderCode())
				&& !"?".equals(evaluateLabClaim.getRequestHeader().getPatientGenderCode())) {
			System.out.println("Gender::::" + evaluateLabClaim.getRequestHeader().getPatientGenderCode());
			claimRequest.setPatientGenderCode(evaluateLabClaim.getRequestHeader().getPatientGenderCode().trim());
		}

		if (null != evaluateLabClaim.getRequestHeader().getPatientDateOfBirth()
				&& !"".equals(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth())
				&& !"?".equals(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth())) {

			claimRequest.setPatientDOB((evaluateLabClaim.getRequestHeader().getPatientDateOfBirth().toString().trim()));
			System.out.println("Patient DOB::::" + evaluateLabClaim.getRequestHeader().getPatientDateOfBirth());
		}

		if ((evaluateLabClaim.getRequestLine().get(0).getUnits()) != 0
				&& !"".equals(evaluateLabClaim.getRequestLine().get(0).getUnits())
				&& !"?".equals(evaluateLabClaim.getRequestLine().get(0).getUnits())) {
			claimRequest.setUnits(evaluateLabClaim.getRequestLine().get(0).getUnits());
			System.out.println("Units::::" + evaluateLabClaim.getRequestLine().get(0).getUnits());
		}

		else {
			claimRequest.setPatientDOB(null);
		}

		claimRequest.setIdCardNumber(evaluateLabClaim.getRequestHeader().getIdCardNumber());
		claimRequest.setEffectiveEndDate(
				sdf.parse(evaluateLabClaim.getRequestLine().get(0).getToDateOfService().toString()));
		int calculatedAge = DateUtility
				.calculateAge(sdf.parse(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth().toString()));
		claimRequest.setCalculatedAge(calculatedAge);
		// claimRequest.setAssociatedMPolicy(evaluateLabClaim.getRequestHeader().get);
		// claimRequest.setInNetworkIndicator(inNetworkIndicator);
		// Sample Response creation for global flow
		UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();

		ResponseHeaderData responseHeaderData = new ResponseHeaderData();

		System.out.println("randomUUIDString : : : " + randomUUIDString);
		responseHeaderData.setCeTransactionId(randomUUIDString);

		responseHeaderData.setClaimNumber(evaluateLabClaim.getRequestHeader().getClaimNumber());
		System.out.println("ClaimNumber::::" + evaluateLabClaim.getRequestHeader().getClaimNumber());
		EvaluateLabClaimResponse evaluateLabClaimResponse = new EvaluateLabClaimResponse();
		SecondaryCodes secondaryCodes = new SecondaryCodes();
		ResponseLineData responseLineData = new ResponseLineData();

		evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
		evaluateLabClaimResponse.getResponseLine().add(responseLineData);

		params.put("claimReq", claimRequest);
		params.put("evaluateLabClaimResponse", evaluateLabClaimResponse);
		params.put("secondaryCodes", secondaryCodes);
		params.put("responseLineData", responseLineData);

		return params;

	}

}